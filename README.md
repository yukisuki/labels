# Install docker (ubuntu 16.04)  
install instructions from:
https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose  

# Curl missing in ubuntu mini 16.04
sudo apt-get install curl  

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"  
sudo apt-get update  
sudo apt-get install -y docker-ce  

# Install docker-compose

install docker-compose  

sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose  
sudo chmod +x /usr/local/bin/docker-compose  

# Setup application

sudo docker-compose build  
sudo docker-compose up -d --force-recreate  

sudo docker-compose exec label-app php artisan key:generate  
sudo docker-compose exec label-app php artisan config:cache  
sudo docker-compose exec label-app php artisan migrate:fresh --seed  

# Running tests
To run tests, in the root directory of the project type:  
./run_tests.sh  

This should run the following command:  
sudo docker-compose exec --user="root" label-app ./vendor/bin/phpunit tests/Feature/Http/Controllers/LabelController.php  

