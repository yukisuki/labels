<?php

namespace App\Http\Controllers;

use App\Label;
use Illuminate\Http\Request;
use Validator;

class LabelController extends Controller {

	public function index() {
		return response()->json(Label::get(), 200);

	}

	public function get($path) {
		$path = '/'.$path;
		$label = Label::where('Path', 'LIKE', $path.'%')->get();
		return response()->json($label, 200);

	}

	// CREATE FUNCTION
	public function create($path = null, Request $req) {
		$formattedPath = '/';
		$Color = $req->Color;
		$BGColor = $req->BGColor;


		$validatorRules = [
			'Name' => 'required'
		];

		$validator = Validator::make($req->all(), $validatorRules);

		if ($validator->fails()) {
			$response = array(
				"status" => "error",
				"message" => $validator->errors(),
				"code" => 400
			);


		}


		if (isset($path)) {
			$formattedPath .= $path;
			$parentPathTest = Label::where('Path', $formattedPath)->get()->toArray();

			if (!isset($parentPathTest[0]['id'])) {
				$response = array(
					"status" => "error",
					"message" => "Parent label does not exist",
					"code" => 400
				);
				return response()->json($response, 400);

			}
			$Color = $parentPathTest[0]['Color'];
			$BGColor = $parentPathTest[0]['BGColor'];


			$formattedPath .= '/';
		}

		$formattedName = preg_replace('/ /', '_', $req->Name);
		$finalPath = $formattedPath.$formattedName;


		$pathTest = Label::where('Path', $finalPath)->get()->toArray();


		if (!empty(array_filter($pathTest))) {
			$response = array(
				"status" => "error",
				"message" => "Label already exists with path",
				"code" => 400
			);
			return response()->json($response, 400);

		}

		$label = Label::create([
				"Name" => $req->Name,
				"Slug" => $formattedName,
				"Path" => $finalPath,
				"Color" => $Color,
				"BGColor" => $BGColor
			]);


		$response = array(
			"status" => "success",
			"data" => $label,
			"code" => 201
		);

		return response()->json($response, 201);


	}








	// UPDATE
	public function update($path, Request $req) {
		$formattedPath = '/'.$path;
		$label = Label::where('Path', $formattedPath);
		$finalPath = '/';

		if (isset($req->Name)) {
			$Name = $req->Name;
			$formattedName = preg_replace('/ /', '_', $Name);
			$pathSplit = preg_split('#/#', $path);
			$pathSliced = array_slice($pathSplit, 0, -1);

			foreach ($pathSliced as $pathPart) {
				$finalPath .= $pathPart.'/';

			}

			$finalPath .= $formattedName;

			$label->update([
					'Name' => $Name,
					'Path' => $finalPath,
					'Slug' => $formattedName

				]);

		}

		$response = array(
			"status" => "success",
			"message" => "label ".$formattedPath." updated",
			"code" => 200
		);


		return response()->json($response, 200);

	}






	// DELET FUNCTION
	public function delete($path, Request $req) {
		$response = null;
		$responseCode = 0;
		$toDelete = Label::where('Path', '/'.$path)->delete();

		if ($toDelete) {
			$responseCode = 200;
			$response = array(
				"status" => "success",
				"code" => $responseCode
			);

		} else {
			$responseCode = 404;
			$response = array(
				"status" => "Not found",
				"code" => $responseCode
			);


		}



		return response()->json($response, $responseCode);


	}

	public function turn() {
		// Label::getQuery()->delete();
		Label::query()->truncate();
		return response()->json('turn\'d', 200);

	}


	public function error(){
		$response = array(
			"status" => "error",
			"message" => "Resource is not implemented",
			"code" => 501
		);
		return response()->json($response, 501);

	}

}
