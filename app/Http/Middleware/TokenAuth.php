<?php

namespace App\Http\Middleware;

use Closure;

class TokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('X-API-TOKEN');

        // if ($token != '4rfg32ferdfsas') {
        //     abort(401, 'Authorization Token not found');
        // }

        return $next($request);
    }
}
