<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Label extends Model {
	protected $fillable = ['Name', 'Slug', 'Path', 'Color', 'BGColor'];

}
