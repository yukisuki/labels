<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// main routes
Route::get('label', 'LabelController@index');
Route::get('label/{path}', 'LabelController@get')->where('path', '(.*)');

Route::post('label/{path?}', 'LabelController@create')->where('path', '(.*)');

Route::put('label/{path}', 'LabelController@update')->where('path', '(.*)');

Route::delete('label/{path}', 'LabelController@delete')->where('path', '(.*)');

Route::any('error', 'LabelController@error');

// debug
Route::get('labelturn', 'LabelController@turn');
