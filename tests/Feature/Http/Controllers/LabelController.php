<?php

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LabelController extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

     // CREATES LABEL HEATING ENGINEER

    public function testCreateNewLabelHE() {
        echo "Creating new Heating Engineer label";
        $res = $this->json('POST', '/api/label', [
            "Name" => "Heating Engineer",
            "Color" => "#000",
            "BGColor" => "#ffa500"
        ]);


        $res->dump();
        $res->assertStatus(201);
    }

    // CREATES LABEL ELECTRICIANS

    public function testCreateNewLabelE() {
        echo "Creating new Electricians label";
        $res = $this->json('POST', '/api/label', [
            "Name" => "Electricians",
            "Color" => "#000",
            "BGColor" => "#0f0",
        ]);

        $res->dump();
        $res->assertStatus(201);

    }

    // CREATES LABEL BOILER UNDER HEATING ENGINEER

    public function testCreateNewLabelBUnderHE() {
        echo "Creating new Boiler label under Heating Engineer";
        $res = $this->json('POST', '/api/label/Heating_Engineer', [
            "Name" => "Boiler"
        ]);

        $res->dump();
        $res->assertStatus(201);

    }

    // CREATES LABELS UNDER HEATING ENGINEER / BOILER

    public function testCreateNewLabelBaxiUnderHEb() {
        echo "Creating new Baxi label under Heating Engineer / Boiler";
        $res = $this->json('POST', '/api/label/Heating_Engineer/Boiler', [
            "Name" => "Baxi"
        ]);

        $res->dump();
        $res->assertStatus(201);

    }


    public function testCreateNewLabelIdealUnderHEb() {
        echo "Creating new Ideal label under Heating Engineer / Boiler";
        $res = $this->json('POST', '/api/label/Heating_Engineer/Boiler', [
            "Name" => "Ideal"
        ]);

        $res->dump();
        $res->assertStatus(201);

    }


    // END OF CREATING LABELS UNDER HEATING ENGINEER / BOILER


    // GET HEATING ENGINEER

    public function testCreateNewLabelCUnderHE() {
        echo "Creating new Cylinder label under Heating Engineer";
        $res = $this->json('POST', '/api/label/Heating_Engineer/Boiler', [
            "Name" => "Cylinder"
        ]);

        $res->dump();
        $res->assertStatus(201);

    }

    // UPDATE IDEAL TO WORCESTER

    public function testUpdateLabelIdeal() {
        echo "Updating label Ideal to Worcester";
        $res = $this->json('PUT', '/api/label/Heating_Engineer/Boiler/Ideal', [
            "Name" => "Worcester"
        ]);

        $resEnd = $this->json('GET', '/api/label');
        $resEnd->dump();

        $res->dump();
        $res->assertStatus(200);

    }


    // DELETE LABEL BAXI

    public function testDeleteLabelBaxi() {
        echo "Deleting label Baxi";
        $res = $this->json('DELETE', '/api/label/Heating_Engineer/Boiler/Baxi', [
            "Name" => "Worcester"
        ]);


        $res->assertStatus(200);

    }

    // RESET DB

    public function testResetDB() {
        echo "Resetting DB";
        $res = $this->json('GET', '/api/labelturn');
        $this->assertTrue(true);
    }

}
